#include "DataLoader.h"

std::vector<std::__cxx11::string> loadLabels(const char *file_path)
{
    std::ifstream file(file_path);
    std::string line_buffer;
    std::vector<std::string> labels;

    if (file && getline(file, line_buffer))
    {
        std::string cell;
        std::stringstream line_stream(line_buffer);

        while (std::getline(line_stream, cell, ','))
        {
            if (cell.front() == '"')
            {
                cell.erase(0, 1); // erase the first character
                cell.erase(cell.size() - 1); // erase the last character
            }
            labels.push_back(cell);
        }
    }

    return labels;
}


std::vector<Point> loadData(const char *file_path, std::set<uint> fields)
{
    std::setlocale(LC_ALL, "C");

    std::ifstream file(file_path);
    std::string line_buffer;
    std::vector<Point> points;

    if (file)
    {
        getline(file, line_buffer);
    }

    while (file && getline(file, line_buffer))
    {
        if (line_buffer.length() == 0)
        {
            continue;
        }

        std::string cell;
        std::string type;
        std::stringstream line_stream(line_buffer);

        auto values = std::vector<double>();

        for(uint i = 0; ; ++i)
        {
            if (!std::getline(line_stream, cell, ','))
            {
                break;
            }
            if (!fields.count(i))
            {
                continue;
            }

            try
            {
                values.push_back(std::stod(cell));
            }
            catch (const std::invalid_argument& ex)
            {
                if (cell.front() == '"')
                {
                    cell.erase(0, 1); // erase the first character
                    cell.erase(cell.size() - 1); // erase the last character
                }
                type = cell;
            }

        }
        points.push_back(Point(values, type));
    }

    file.close();

    return points;

}
