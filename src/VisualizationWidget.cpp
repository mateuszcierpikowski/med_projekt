#include "VisualizationWidget.h"

VisualizationWidget::VisualizationWidget(QWidget *parent): QChartView(parent)
{
    colors[0][0] = QColor(255, 0, 0);
    colors[0][1] = QColor(255, 100, 0);
    colors[0][2] = QColor(255, 0, 100);

    colors[1][0] = QColor(0, 0, 255);
    colors[1][1] = QColor(0, 150, 255);
    colors[1][2] = QColor(100, 0, 255);


    colors[2][0] = QColor(0, 150, 0);
    colors[2][1] = QColor(150, 255, 0);
    colors[2][2] = QColor(0, 255, 70);
}

void VisualizationWidget::clearChart()
{
    chart()->removeAllSeries();
    chart()->setAxisX(new QValueAxis);
    chart()->setAxisY(new QValueAxis);
}

void VisualizationWidget::drawChart(const std::vector<std::unique_ptr<Cluster>> &clusters,
                                    std::map<std::string, int> types, const std::vector<std::string> &labels,
                                    const std::vector<uint> & labels_numbers)
{
    clearChart();
    uint i = 0;
    auto max_x = std::numeric_limits<double>::min();
    auto max_y = std::numeric_limits<double>::min();

    auto min_x = std::numeric_limits<double>::max();
    auto min_y = std::numeric_limits<double>::max();

    for (const auto& cluster: clusters)
    {
        for (const auto& point: cluster->getPoints())
        {
            types[point.getType()] += 1;
        }

        std::map<std::string, QScatterSeries*> series_map;

        uint j = 0;
        for (auto& type: types)
        {
            if (type.second > 0)
            {
                QScatterSeries *series = new QScatterSeries();
                series->setName(QString::number(i+1) + "." + QString::fromStdString(type.first));
                QColor color = colors[i%3][j%3];
                series->setColor(color);
                series->setMarkerSize(10.0);

                if (i % 2 == 0)
                {
                    series->setMarkerShape(QScatterSeries::MarkerShapeRectangle);
                }
                else
                {
                    series->setMarkerShape(QScatterSeries::MarkerShapeCircle);
                }

                series_map.insert({type.first, series});
                ++j;
            }
            type.second = 0;
        }

        for (const auto& point: cluster->getPoints())
        {
            series_map[point.getType()]->append(point.getAxisValues().at(labels_numbers.at(0)),
                                                point.getAxisValues().at(labels_numbers.at(1)));
            max_x = std::max(max_x, point.getAxisValues().at(labels_numbers.at(0)));
            max_y = std::max(max_y, point.getAxisValues().at(labels_numbers.at(1)));

            min_x = std::min(min_x, point.getAxisValues().at(labels_numbers.at(0)));
            min_y = std::min(min_y, point.getAxisValues().at(labels_numbers.at(1)));
        }
        for (auto& series: series_map)
        {
            chart()->addSeries(series.second);
        }
        ++i;
    }
    chart()->createDefaultAxes();

    chart()->axisX()->setRange(min_x - 1, max_x + 1);
    chart()->axisY()->setRange(min_y - 1, max_y + 1);

    chart()->axisX()->setTitleText(QString::fromStdString(labels.at(labels_numbers.at(0))));
    chart()->axisY()->setTitleText(QString::fromStdString(labels.at(labels_numbers.at(1))));

    connectMarkers();
}


void VisualizationWidget::connectMarkers()
{
    // Connect all markers to handler
    foreach (QLegendMarker* marker, chart()->legend()->markers())
    {
        // Disconnect possible existing connection to avoid multiple connections
        QObject::disconnect(marker, SIGNAL(clicked()), this, SLOT(handleMarkerClicked()));
        QObject::connect(marker, SIGNAL(clicked()), this, SLOT(handleMarkerClicked()));
    }
}

void VisualizationWidget::disconnectMarkers()
{
    foreach (QLegendMarker* marker, chart()->legend()->markers())
    {
        QObject::disconnect(marker, SIGNAL(clicked()), this, SLOT(handleMarkerClicked()));
    }
}

void VisualizationWidget::handleMarkerClicked()
{
    QLegendMarker* marker = qobject_cast<QLegendMarker*> (sender());
    Q_ASSERT(marker);

    // Toggle visibility of series
    marker->series()->setVisible(!marker->series()->isVisible());

    // Turn legend marker back to visible, since hiding series also hides the marker
    // and we don't want it to happen now.
    marker->setVisible(true);

    // Dim the marker, if series is not visible
    qreal alpha = 1.0;

    if (!marker->series()->isVisible())
    {
        alpha = 0.5;
    }

    QColor color;
    QBrush brush = marker->labelBrush();
    color = brush.color();
    color.setAlphaF(alpha);
    brush.setColor(color);
    marker->setLabelBrush(brush);

    brush = marker->brush();
    color = brush.color();
    color.setAlphaF(alpha);
    brush.setColor(color);
    marker->setBrush(brush);

    QPen pen = marker->pen();
    color = pen.color();
    color.setAlphaF(alpha);
    pen.setColor(color);
    marker->setPen(pen);
}
