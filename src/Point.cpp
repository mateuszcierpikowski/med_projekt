#include "Point.h"

Point::Point(std::vector<double> axisValues, std::string type)
    : axisValues(axisValues), type(std::move(type))
{

}

Point::~Point() {}

double Point::GetDistance(const Point &point, int minkowskyValue) const
{
    double dimensionsSum = 0.0;

    for (uint i = 0; i < axisValues.size(); ++i)
    {
        dimensionsSum += pow(fabs(axisValues[i] - point.axisValues[i]), (double)minkowskyValue);
    }

    return pow(dimensionsSum, 1/(double)minkowskyValue);
}

int Point::getValuesSize() const
{
    return axisValues.size();
}

void Point::setType(std::string type)
{
    this->type = type;
}

const std::string & Point::getType() const
{
    return type;
}

const std::vector<double> &Point::getAxisValues() const
{
    return axisValues;
}

