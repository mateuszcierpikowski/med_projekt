#include "Cluster.h"
#include <iostream>

Cluster::Cluster(std::vector<Point> initialPoints)
    : points(std::move(initialPoints)), joined(false)
{

}

Cluster::Cluster(std::unique_ptr<Cluster>&& cluster_a, std::unique_ptr<Cluster>&& cluster_b) :
        joined_cluster_a(std::move(cluster_a)),
        joined_cluster_b(std::move(cluster_b)),
        joined(true)
{
    points.insert(points.end(), joined_cluster_a->getPoints().begin(), joined_cluster_a->getPoints().end());
    points.insert(points.end(), joined_cluster_b->getPoints().begin(), joined_cluster_b->getPoints().end());
}

Cluster::~Cluster()
{
}

Cluster::Cluster(Cluster &&cluster):
        points(std::move(cluster.points)),
        joined_cluster_a(std::move(joined_cluster_a)),
        joined_cluster_b(std::move(joined_cluster_b))
{

}

double Cluster::CalculateLinkTo(const Cluster &cluster, LinkType type)
{
    double ret = 0.0;
    switch (type)
    {
        case LinkType::SINGLE:
            ret = std::numeric_limits<double>::max();

            for (const auto& this_point : points)
            {
                for (const auto& point : cluster.getPoints())
                {
                    ret = std::min(ret, this_point.GetDistance(point));
                }
            }
            break;
        case LinkType::COMPLETE:
            ret = std::numeric_limits<double>::min();

            for (const auto& this_point : points)
            {
                for (const auto& point : cluster.getPoints())
                {
                    ret = std::max(ret, this_point.GetDistance(point));
                }
            }
            break;
        case LinkType::AVERAGE:
            for (const auto& this_point : points)
            {
                for (const auto& point : cluster.getPoints())
                {
                    ret += this_point.GetDistance(point);
                }
            }
            ret /= (points.size() * cluster.getPoints().size());
            break;

    }
    return ret;
}

const std::vector<Point>& Cluster::getPoints() const
{
    return points;
}

LinkType Cluster::getLinkTypeFromString(std::string link)
{
    if (link == "complete")
    {
        return LinkType::COMPLETE;
    }
    else if (link == "single")
    {
        return LinkType::SINGLE;
    }
    else
    {
        return LinkType::AVERAGE;
    }
}
