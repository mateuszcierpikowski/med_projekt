#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionOpen, SIGNAL(triggered(bool)), this, SLOT(onOpenClicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onOpenClicked()
{
    data_file_path = QFileDialog::getOpenFileName(this,
                                                 tr("Open data"), "/home/mati/med_projekt/data", tr("Data Files (*.csv)"));

    if (data_file_path.isEmpty())
        return;

    labels = loadLabels(data_file_path.toStdString().c_str());

    ui->labels->clear();
    ui->axis_label->clear();

    for (const auto& label: labels)
    {
        auto *it = new QListWidgetItem(ui->labels);
        auto check_box = new QCheckBox(tr(label.c_str()));
        check_box->setChecked(true);
        ui->labels->setItemWidget(it, check_box);

        auto *it_axis_label = new QListWidgetItem(ui->axis_label);
        auto check_box_axis_label = new QCheckBox(tr(label.c_str()));
        ui->axis_label->setItemWidget(it_axis_label, check_box_axis_label);
    }

}

void MainWindow::on_clustering_clicked()
{
    chart_labels_numbers.clear();

    std::set<uint> labels_numbers;
    for (int i = 0; i < ui->labels->count(); ++i)
    {
        auto* item = ui->labels->item(i);
        auto* widget = (QCheckBox*)ui->labels->itemWidget(item);

        auto* item_axis = ui->axis_label->item(i);
        auto* widget_axis = (QCheckBox*)ui->axis_label->itemWidget(item_axis);

        if (widget->isChecked())
        {
            labels_numbers.insert(i);
            if (widget_axis->isChecked())
            {
                chart_labels_numbers.push_back(labels_numbers.size() - 1);
            }
        }
    }

    if (labels_numbers.size() < 2)
    {
        QMessageBox message_box;
        message_box.critical(0,"Error", "Check more than two labels");
        return;
    }

    auto points = loadData(data_file_path.toStdString().c_str(), labels_numbers);

    types.clear();

    for (const auto& point: points)
    {
        types.insert({point.getType(), 0});
    }

    if (chart_labels_numbers.size() != 2)
    {
        QMessageBox message_box;
        message_box.critical(0,"Error", "Check two axis labels");

        return;
    }

    doMagic(hierarchical_clustering_single, points, ui->single_table, ui->visualization_single,
            ui->precision_single, ui->accuracy_single);
    doMagic(hierarchical_clustering_complete, points, ui->complete_table, ui->visualization_complete,
            ui->precision_complete, ui->accuracy_complete);
    doMagic(hierarchical_clustering_average, points, ui->average_table, ui->visualization_average,
            ui->precision_average, ui->accuracy_average);
}

void MainWindow::doMagic(HierarchicalClustering &clustering, const std::vector<Point> points, QTableView* table_view,
                         VisualizationWidget* visual_widget, QLabel* precision_label, QLabel* accuracy_label)
{
    clustering.setData(points);
    clustering.setMinNumberOfGroup(ui->max_number->value());
    clustering.doClustering();

    auto* model = new QStandardItemModel(types.size(), types.size(), this);

    table_view->setModel(model);

    int i = 0;
    for (auto& type: types)
    {
        model->setVerticalHeaderItem(i, new QStandardItem(QString::fromStdString(type.first)));
        ++i;
    }

    int sum_of_table = 0;
    int accuracy_corr = 0;

    std::map<std::string, std::pair<int, int>> precision; // class name, <correct row value, whole column sum>
    bool empty_class = false;

    int column = 0;
    for (const auto& cluster: clustering.getClusters())
    {
        int max_value_in_cluster = 0;
        int sum_in_cluster = 0;
        std::string cluster_class;
        for (const auto& point: cluster->getPoints())
        {
            types[point.getType()] += 1;
        }

        int row = 0;
        for (auto& type: types)
        {
            if (type.first == "")
                empty_class = true;
            else {
                max_value_in_cluster = std::max(max_value_in_cluster, type.second);
                if (max_value_in_cluster == type.second)
                    cluster_class = type.first;
                sum_in_cluster += type.second;
            }
            QStandardItem *item = new QStandardItem(QString::number(type.second));
            item->setEditable(false);

            model->setItem(row, column, item);

            type.second = 0;
            ++row;
        }
        ++column;

        if (!empty_class) {
            sum_of_table += sum_in_cluster;
            accuracy_corr += max_value_in_cluster;
            if (precision.find(cluster_class) == precision.end())
                precision[cluster_class] = {max_value_in_cluster, sum_in_cluster};
            else
                precision[cluster_class] = {precision[cluster_class].first + max_value_in_cluster, precision[cluster_class].second + sum_in_cluster};
        }
    }

    std::string precision_summary;
    if (!empty_class) {
    for (auto &p: precision)
        precision_summary += p.first + " = " + std::to_string(p.second.first/(double)p.second.second) + "; ";
    } else {
        precision_summary = " - ";
    }
    table_view->setModel(model);

    visual_widget->drawChart(clustering.getClusters(), types, labels, chart_labels_numbers);
    precision_label->setText(QString::fromStdString(precision_summary));

    if (!empty_class)
        accuracy_label->setText(QString::number(accuracy_corr/(double)sum_of_table));
    else
        accuracy_label->setText(QString::fromStdString(" - "));
}
