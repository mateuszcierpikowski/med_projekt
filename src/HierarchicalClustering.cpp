//
// Created by mati on 14.12.16.
//

#include "HierarchicalClustering.h"

HierarchicalClustering::HierarchicalClustering(LinkType link_type):
    link_type(link_type)
{

}

void HierarchicalClustering::setData(std::vector<Point> data)
{
    this->data = std::move(data);
    clusters.clear();
    for(const auto& point: this->data)
    {
        clusters.emplace_back(new Cluster(std::vector<Point>{point}));
    }
}

void HierarchicalClustering::setLinkType(LinkType link_type)
{
    this->link_type = link_type;
}

void HierarchicalClustering::doClustering()
{
    while (clusters.size() > min_number_of_group)
    {
        auto value = std::numeric_limits<double>::max();

        uint first_join_cluster = 0;
        uint second_join_cluster = 0;

        for(uint i = 0; i < clusters.size(); ++i)
        {
            for(uint j = i + 1; j < clusters.size(); ++j)
            {
                if (value > clusters.at(i)->CalculateLinkTo(*clusters.at(j), link_type))
                {
                    first_join_cluster = i;
                    second_join_cluster = j;
                    value = clusters.at(i)->CalculateLinkTo(*clusters.at(j), link_type);
                }
            }
        }

        clusters.emplace_back(new Cluster(std::move(clusters.at(first_join_cluster)),
                                          std::move(clusters.at(second_join_cluster))));

        clusters.erase(std::remove_if(clusters.begin(),
                                      clusters.end(),
                                      [](const std::unique_ptr<Cluster>& cluster){return cluster == nullptr;}),
                       clusters.end());

    }
}

void HierarchicalClustering::setMinNumberOfGroup(uint min_number_of_group)
{
    this->min_number_of_group = min_number_of_group;
}

const std::vector<std::unique_ptr<Cluster>>& HierarchicalClustering::getClusters() const
{
    return clusters;
}
