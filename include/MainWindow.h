#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QStringListModel>
#include <QCheckBox>
#include <QWidgetList>
#include <QStandardItemModel>
#include <QTableView>
#include <QtCharts/QtCharts>
#include <QGraphicsView>
#include <QMessageBox>

#include <iostream>
#include <map>

#include "DataLoader.h"
#include "HierarchicalClustering.h"
#include "VisualizationWidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onOpenClicked();

private slots:
    void on_clustering_clicked();

private:
    Ui::MainWindow *ui;
    QString data_file_path;
    HierarchicalClustering hierarchical_clustering_single{LinkType::SINGLE};
    HierarchicalClustering hierarchical_clustering_average{LinkType::AVERAGE};
    HierarchicalClustering hierarchical_clustering_complete{LinkType::COMPLETE};
    std::map<std::string, int> types;
    std::vector<std::string> labels;
    std::vector<uint> chart_labels_numbers;

    void doMagic(HierarchicalClustering& clustering, const std::vector<Point> points,
                 QTableView* table_view, VisualizationWidget* visual_widget,
                 QLabel *precision_label, QLabel *accuracy_label);
};

#endif // MAINWINDOW_H
