//
// Created by mati on 13.12.16.
//

#ifndef HIERARCHICALCLUSTERING_DATALOADER_H
#define HIERARCHICALCLUSTERING_DATALOADER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <clocale>

#include "Point.h"

std::vector<std::string> loadLabels(const char* file_path);

std::vector<Point> loadData(const char* file_path, std::set<uint> fields);

#endif //HIERARCHICALCLUSTERING_DATALOADER_H
