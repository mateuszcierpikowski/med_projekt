#ifndef HIERARCHICALCLUSTERING_POINT_H
#define HIERARCHICALCLUSTERING_POINT_H

#include <vector>
#include <math.h>
#include <string>

class Point
{
public:
    Point (std::vector<double> axisValues, std::string type = std::string());
    double GetDistance(const Point &point, int minkowskyValue = 2) const;
    virtual ~Point ();
    int getValuesSize() const;
    void setType(std::string type);
    const std::string& getType() const;
    const std::vector<double>& getAxisValues() const;

private:
    std::vector<double> axisValues;
    std::string type;
};

#endif //HIERARCHICALCLUSTERING_POINT_H
