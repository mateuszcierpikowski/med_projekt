#ifndef HIERARCHICALCLUSTERING_CLUSTER_H
#define HIERARCHICALCLUSTERING_CLUSTER_H

#include <limits>
#include <vector>
#include <memory>

#include "Point.h"

enum LinkType {
    SINGLE,
    COMPLETE,
    AVERAGE
};

class Cluster
{
public:
    Cluster(std::vector<Point> initialPoints);
    Cluster(std::unique_ptr<Cluster>&& cluster_a, std::unique_ptr<Cluster>&& cluster_b);
    Cluster(const Cluster&) = delete;
    Cluster(Cluster&& cluster);
    virtual ~Cluster ();
    double CalculateLinkTo(const Cluster &cluster, LinkType type);
    const std::vector<Point>& getPoints() const;
    static LinkType getLinkTypeFromString(std::string link_type);
private:
    std::vector<Point> points;
    std::unique_ptr<Cluster> joined_cluster_a;
    std::unique_ptr<Cluster> joined_cluster_b;
    bool joined;
};

#endif //HIERARCHICALCLUSTERING_CLUSTER_H
