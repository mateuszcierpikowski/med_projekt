//
// Created by mati on 14.12.16.
//

#ifndef HIERARCHICALCLUSTERING_HIERARCHICALCLUSTERING_H
#define HIERARCHICALCLUSTERING_HIERARCHICALCLUSTERING_H

#include <algorithm>
#include <memory>

#include "Cluster.h"


class HierarchicalClustering
{
public:
    HierarchicalClustering() = default;
    HierarchicalClustering(LinkType link_type);
    void doClustering();
    void setLinkType(LinkType link_type);
    void setData(std::vector<Point> data);
    void setMinNumberOfGroup(uint min_number_of_group);
    const std::vector<std::unique_ptr<Cluster>>& getClusters() const;
private:
    LinkType link_type;
    std::vector<Point> data;
    std::vector<std::unique_ptr<Cluster>> clusters;
    uint min_number_of_group{1};
};


#endif //HIERARCHICALCLUSTERING_HIERARCHICALCLUSTERING_H
