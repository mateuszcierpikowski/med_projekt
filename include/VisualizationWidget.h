#ifndef VISUALIZATIONWIDGET_H
#define VISUALIZATIONWIDGET_H

#include <QWidget>
#include <QChartView>
#include <QChart>
#include <QScatterSeries>
#include <QLegendMarker>
#include <QValueAxis>

#include <cmath>
#include <iostream>


#include "Cluster.h"

using namespace QtCharts;

class VisualizationWidget : public QChartView
{
    Q_OBJECT
public:
    explicit VisualizationWidget(QWidget *parent = 0);
private:
    std::map<int, std::map<int, QColor>> colors;
signals:

public slots:
    void clearChart();
    void drawChart(const std::vector<std::unique_ptr<Cluster>>& clusters, std::map<std::string, int> types,
                   const std::vector<std::string>& labels, const std::vector<uint> &labels_numbers);
    void connectMarkers();
    void disconnectMarkers();

    void handleMarkerClicked();
};

#endif // VISUALIZATIONWIDGET_H
