#-------------------------------------------------
#
# Project created by QtCreator 2016-12-15T19:25:15
#
#-------------------------------------------------

CONFIG += c++11


QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += charts

TARGET = HierarchicalClustering
TEMPLATE = app

INCLUDEPATH += $$PWD/include

SOURCES += main_gui.cpp \
        src/MainWindow.cpp \
        src/Cluster.cpp \
        src/HierarchicalClustering.cpp \
        src/Point.cpp \
        src/DataLoader.cpp \
        src/VisualizationWidget.cpp

HEADERS  += include/MainWindow.h \
            include/Cluster.h \
            include/DataLoader.h \
            include/HierarchicalClustering.h \
            include/Point.h \
            include/VisualizationWidget.h

FORMS    += ui/MainWindow.ui

