#include <iostream>
#include <map>

#include "HierarchicalClustering.h"
#include "DataLoader.h"

using namespace std;


int main(int argc, char *argv[])
{
    HierarchicalClustering hierarchical_clustering;

    hierarchical_clustering.setData(loadData(argv[1], {0, 1, 2, 3, 4}));
    hierarchical_clustering.setLinkType(Cluster::getLinkTypeFromString(argv[2]));
    hierarchical_clustering.setMinNumberOfGroup(3);

    std::map<std::string, int> types;
    types.insert({"setosa", 0});
    types.insert({"virginica", 0});
    types.insert({"versicolor", 0});

    hierarchical_clustering.doClustering();

    for (const auto& cluster: hierarchical_clustering.getClusters())
    {
        std::cout<<"-------------------"<<std::endl;

        for (const auto& point: cluster->getPoints())
        {
            types[point.getType()] += 1;
        }
        for (auto& type: types)
        {
            std::cout<<type.first<<": "<<type.second<<std::endl;
            type.second = 0;
        }
    }

    return 0;
}
