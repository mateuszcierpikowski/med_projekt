#define BOOST_TEST_MODULE SamepleTest
#include <boost/test/unit_test.hpp>
#include <math.h>

#include "Point.h"

BOOST_AUTO_TEST_CASE(TestName)
{
    {
        Point point1({0.0, 0.0});
        Point point2({1.0, 1.0});

        double distance = point1.GetDistance(point2);
        BOOST_CHECK_EQUAL(distance, sqrt(2.0));

        distance = point1.GetDistance(point2, 1);
        BOOST_CHECK_EQUAL(distance, 2.0);
    }

    {
        Point point1({0.0, 0.0});
        Point point2({2.0, 2.0});

        double distance = point1.GetDistance(point2);
        BOOST_CHECK_EQUAL(distance, 2*sqrt(2));

        distance = point1.GetDistance(point2, 1);
        BOOST_CHECK_EQUAL(distance, 4.0);
    }

    {
        Point point1({0.0, 0.0, 0.0});
        Point point2({1.0, 1.0, 1.0});

        double distance = point1.GetDistance(point2);
        BOOST_CHECK_EQUAL(distance, sqrt(3.0));

        distance = point1.GetDistance(point2, 1);
        BOOST_CHECK_EQUAL(distance, 3.0);
    }
}
