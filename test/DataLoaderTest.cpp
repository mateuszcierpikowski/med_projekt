#define BOOST_TEST_MODULE ClusterTest
#include <boost/test/unit_test.hpp>

#include "DataLoader.h"

BOOST_AUTO_TEST_CASE(DataLoaderTest)
{
    {
        auto points = loadData("../data/iris.csv", {0, 1, 2, 3, 4});

        BOOST_CHECK_EQUAL(150, points.size());

        BOOST_CHECK_EQUAL(4, points.at(0).getValuesSize());
        BOOST_CHECK_EQUAL(4, points.at(0).getValuesSize());
        BOOST_CHECK_EQUAL("setosa", points.at(0).getType());
    }

    {
        auto labels = loadLabels("../data/iris.csv");
//        std::vector<std::string> act_labels{,"Sepal.Width","Petal.Length","Petal.Width","Species"};

        BOOST_CHECK_EQUAL("Sepal.Length", labels.at(0));
    }
}
