#define BOOST_TEST_MODULE ClusterTest
#include <boost/test/unit_test.hpp>
#include <math.h>

#include "Cluster.h"

BOOST_AUTO_TEST_CASE(LinkageTest)
{
    {
        std::vector<Point> pointsGroup = {{ {0.0, 0.0}}, {{0.0, 1.0} }};
        std::vector<Point> pointsGroup2 = {{ {2.0, 0.0}}, {{2.0, 1.0} }};
        Cluster cluster1(pointsGroup);
        Cluster cluster2(pointsGroup2);

        double maximumLinkage = cluster1.CalculateLinkTo(cluster2, COMPLETE);
        BOOST_CHECK_EQUAL(maximumLinkage, sqrt(5.0));

        double singleLinkage = cluster1.CalculateLinkTo(cluster2, SINGLE);
        BOOST_CHECK_EQUAL(singleLinkage, 2);

        double averageLinkage = cluster1.CalculateLinkTo(cluster2, AVERAGE);
        BOOST_CHECK_EQUAL(averageLinkage, (2*2 + 2*sqrt(5.0))/4);
    }

}
