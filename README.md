1. Pobieramy najnowszą bibliotekę Qt: https://www.qt.io/download-open-source/#section-2
2. Przechodzimy do folderu głównego projektu oraz tworzymy folder do budowania
3. Włączamy konfigurację projektu za pomocą programu: qmake dołączonego do zainstalowanej biblioteki Qt, podając jako parametr plik 
   HierarchicalClustering.pro, w moim przypadku było to: ~/tools/Qt5.7.1/5.7/gcc_64/bin/qmake ../HierarchicalClustering.pro
4. Uruchamiamy program: make
5. Włączamy aplikacje: ./HierarchicalClustering 

